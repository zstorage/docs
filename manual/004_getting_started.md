*责任人：杨磊*

## 1. 启动集群

## 2. 添加节点

## 3. 创建存储池

## 4. 格式化硬盘

## 5. 添加硬盘

## 6. 添加OSD硬盘

## 7. 创建卷

## 8. 添加主机

## 9. 配置NVMe over TCP listener

## 10. 配置LUN

## 11. 主机上链接zstorage

## 12. 主机上配置多路径

## 13. 主机格式化zstorage的卷(mkfs.ext4)

## 14. 主机mount zstorage的卷


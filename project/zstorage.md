
### 1. zStorage
zStorage 是云和恩墨针对数据库应用开发的高性能全闪分布式块存储。zStorage 支持
多存储池、精简配置、快照/一致性组快照、链接克隆/完整克隆、NVMeoF/NVMeoTCP、
iSCSI、CLI和API管理、快照差异位图（DCL）、慢盘检测、亚健康管理、16KB原子写、
2副本、强一致3副本、Raft 3副本、IB和RoCE、TCP/IP、后台巡检、
基于Merkle树的一致性校验、全流程TRIM、QoS、SCSI PR、SCSI CAW。

### 组织介绍
本账号是zStorage社区版的运营账号，用来提交Bug，使用问题求助等。

本账号也用来开发最新版本文档。

### 如何加入
请发送申请邮件至 yan.huang (at) enmotech.com

### 联系
网站：
www.enmotech.com
邮箱:
yan.huang (at) enmotech.com


